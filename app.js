var express = require('express');
var app = express.createServer();
var io = require("socket.io");
var adamasmaca = io.listen(app);

// Settings
var gameID = 0;
var word = "The Hobbit The Desolation of Smaug";
var guessed = "__________________________________";

// Routes
app.use(express.bodyParser());
app.get('/', function(req, res) {
	res.render('giris.ejs');
});
app.post('/', function(req, res) {
	if(req.body.creategame)
	{
		gameID = Math.floor((Math.random() * 100) + 1);
		console.log("Game ID : "+gameID);
		res.redirect('/game');
	}
	else if(req.body.joingame)
	{
		gameID = req.body.gameID;
		res.redirect('/game');
	}

	res.end();
});
app.get('/game', function(req,res) {
	res.render('game.ejs', {gameID : gameID});
	res.end();
});

adamasmaca.on('connection', function(socket) {
	socket.on('guess', function(d) {
		var wordLC = word.toLowerCase();
		var pos = wordLC.indexOf(String.fromCharCode(d.key));
		if(pos > -1)
		{
			// found it!
			adamasmaca.sockets.emit('found', {key:d.key, result:movieName(word,d.key), uID:d.uID});
		}
		else
		{
			adamasmaca.sockets.emit('notfound', {key:d.key, result:movieName(word,d.key), uID:d.uID});
		}
		adamasmaca.sockets.emit('guess', {key:d.key, result:movieName(word,d.key), uID:d.uID});
	});
});

function movieName(name, guess)
{
	var wordLC = name.toLowerCase();
	guess = String.fromCharCode(guess);
	
	for(var x = 0;x<wordLC.length;x++)
	{
		if(wordLC.charAt(x) == guess)
		{ 
			guessed = guessed.replaceAt(x, guess);
		}
		
	}
	return guessed;
}
// http://stackoverflow.com/a/1431113
String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

app.listen(3000);